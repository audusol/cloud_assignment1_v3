package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)


type Country struct {
	Code string `json:"alpha2Code"`
	Name string `json:"name"`
	Flag string `json:"flag"`
	Species []string `json:"species"`
	SpeciesKey []int `json:"speciesKey"`
}

type NameKeyPair struct {
	Name string `json:"species"`
	Key int `json:"speciesKey"`
}

type Result struct {
	Results []NameKeyPair `json:"results"`
}

type Species struct {
	Key int `json:"key"`
	Kingdom string `json:"kingdom"`
	Phylum string `json:"phylum"`
	Order string `json:"order"`
	Family string `json:"family"`
	Genus string `json:"genus"`
	ScientificName string `json:"scientificName"`
	CanonicalName string `json:"canonicalName"`
	Year string `json:"year"`
}

type speciesYear struct {
	Year string `json:"year"`
	BracketYear string `json:"bracketYear"`
}

type Diagnostic struct {

	StatusGBIF int `json:"gbif"`
	RestCountry int `json:"restcountries"`
	Version string `json:"version"`
	Uptime int64 `json:"uptime"`
}

func defaultHandler (w http.ResponseWriter, r *http.Request) {
	helloString := "This is the page of Audun Landøy Solli Bitsec"
	fmt.Printf(helloString)
}

func countryHandler (w http.ResponseWriter, r *http.Request) {

	// The URL is 25 + key long, so I skip the 25 first keys
	countryCode := r.URL.Path[25:]

	// Country codes are two characters long, if not Error
	if len(countryCode) != 2 {
		http.Error(w, "Wrong country code used", http.StatusBadRequest)
	}
	// Pull the country information from the website
	respCountry, err := http.Get(fmt.Sprintf("http://restcountries.eu/rest/v2/alpha/%s", countryCode))
	if err != nil {
		log.Fatalln(err)
	}

	var countryData Country
	var resultData Result

	//  Since I don't actually care about the error code, I use _
	_ = json.NewDecoder(respCountry.Body).Decode(&countryData)

					// Since the default limit for lookups is 20, I figured the best way to include
	limit := 20		// both a limit and not, is to set the limit to 20. That way I can do both limit-checks and
	offset := 0		// no-limit-checks.

	if r.URL.Query()["limit"] != nil {		// see if limit is in the URL

		customLimit := r.URL.Query()["limit"][0]			// finds the string of the "number"
		customLimitInt, err := strconv.Atoi(customLimit)	// converts from string to int

		if err == nil {
			limit = customLimitInt							// if all goes well, we now have a good limit
		} else {
			log.Fatalln(err)
		}
	}	//and if the user doesn't write a limit, we use limit = 20, with the user noticing no difference


	for ; limit > 0; {
		respSpecies, err := http.Get(fmt.Sprintf("http://api.gbif.org/v1/occurrence/search?country=%s&limit=%d&offset=%d", countryCode, limit, offset))
		if err != nil {
			log.Fatalln(err)
		}

		_ = json.NewDecoder(respSpecies.Body).Decode(&resultData)


		limit -= 300
		offset += 300
		exist := false
		for i := 0 ; i < len(resultData.Results) ; i++ {

			for j := 0; j < len(countryData.SpeciesKey); j++ {
				if resultData.Results[i].Key == countryData.SpeciesKey[j]{
					exist = true
				}
			}

		if exist == false {
			countryData.SpeciesKey = append(countryData.SpeciesKey, resultData.Results[i].Key)
			countryData.Species = append(countryData.Species, resultData.Results[i].Name)
		}
		exist = false

		}

	}

	// This prints the json struct to the website
	w.Header().Add("content-type", "application/json")
	_ = json.NewEncoder(w).Encode(countryData)
}

func speciesHandler (w http.ResponseWriter, r *http.Request) {

	// The URL is 25 + key long, so I skip the 25 first keys
	speciesKey := r.URL.Path[25:]

	// If there is no key, I cannot do the work. Error
	if len(speciesKey) == 0 {
		http.Error(w, "No species key found", http.StatusBadRequest)
	}
	// Gets the species information specified by the user
	resp, err := http.Get(fmt.Sprintf("http://api.gbif.org/v1/species/%s", speciesKey))
	if err != nil {
		log.Fatalln(err)
	}
	//  Since I don't actually care about the error code, I use _
	var speciesData Species
	_ = json.NewDecoder(resp.Body).Decode(&speciesData)

	// getting year
	resp, err = http.Get(fmt.Sprintf("http://api.gbif.org/v1/species/%s/name", speciesKey))
	if err != nil {
		log.Fatalln(err)
	}

	// This enters the name page and finds information about the year
	// The page should have two types of year, a Year and a BracketYear, why this is, I don't know
	var Year speciesYear
	_ = json.NewDecoder(resp.Body).Decode(&Year)

	// We first check if BracketYear is empty, if it is, we use Year, if it has a value, we use that instead.
	// If both are blank then there is no year and it remains blank.
	if Year.BracketYear == "" {
		speciesData.Year = Year.Year
	} else {
		speciesData.Year = Year.BracketYear
	}

	w.Header().Add("content-type", "application/json")
	_ = json.NewEncoder(w).Encode(speciesData)

}

func diagHandler (w http.ResponseWriter, r *http.Request) {

	// I know https://api.gbif.org/v1/ is always up, so if I get the data, it is working
	gbifResp, err := http.Get("http://api.gbif.org/v1/")
	if err != nil {
		log.Fatalln(err)
	}
	
	// Same as above
	restResp, err := http.Get("http://restcountries.eu/rest/v2/")
	if err != nil {
		log.Fatalln(err)
	}

	// I save them away to give to the diagData struct
	gbifStatus := gbifResp.StatusCode
	restCountryStatus := restResp.StatusCode

	// I take a new timestamp at this point and find the difference between now and the old time
	newTime := time.Now()
	bigTime := int64(newTime.Sub(upTime) / time.Second)

	diagData := Diagnostic{gbifStatus, restCountryStatus, "v1", bigTime}

	// This prints the json struct to the website
	w.Header().Add("content-type", "application/json")
	_ = json.NewEncoder(w).Encode(diagData)

}

var upTime = time.Now() // this takes a timestamp at the start of the program

func main () {

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		//log.Fatal("$PORT must be set")
	}

	//will print port number to heroku logs --tail
	fmt.Println(port)


	//http.HandleFunc("/conservation/v1/", defaultHandler)
	http.HandleFunc("/conservation/v1/country/" , countryHandler)
	http.HandleFunc("/conservation/v1/species/", speciesHandler)
	http.HandleFunc("/conservation/v1/diag/", diagHandler)

	//log.Fatal(http.ListenAndServe(":8080", nil))
	log.Fatal(http.ListenAndServe(":" + port, nil))
}

